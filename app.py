# нахождение корней квадратного уравнения
# ax2 + bx + c = 0
import math # Для извлечения кв.корня
a = 4
b = -7
c = -2
# 4x2 - 7x - 2 = 0
D = b**2 - 4 * a * c # находим D уравнения.
if D < 0:
    print('Корней нет')
elif D == 0:
    print('Корень только один:' + str(-b / (2*a)))
else:
    print('x1= ' + str((-b + math.sqrt(D)) / (2 * a)))
    print('x2= ' + str((-b - math.sqrt(D)) / (2 * a)))